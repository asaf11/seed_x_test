# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views import View
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse

from .models import ReportEntry, Report, User, Machine, CropTrait


def index(request):
    users = User.objects.all()
    machines = Machine.objects.all()
    crop_traits = CropTrait.objects.all()
    reports = Report.objects.all().select_related()

    return render(request, 'index.html', {
        'users': users,
        'machines': machines,
        'crop_traits': crop_traits,
        'reports': reports
    })


def create_report(request):
    user_id = request.POST['user_id']
    machine_id = request.POST['machine_id']
    ct_id = request.POST['crop_trait_id']

    Report.objects.create(user_id=user_id, machine_id=machine_id, crop_trait_id=ct_id)
    return HttpResponseRedirect('/')


def upload(request):
    img_file = request.FILES['img']  # TODO: validate the file
    report_id = request.POST['report_id']

    # set a dummy answer
    ReportEntry.objects.create(report_id=report_id, img=img_file, answer="")
    
    return HttpResponseRedirect('/')


class ImagesByUser(View):
    def get(self, request, *args, **kwargs):
        """ Returns a JSON with image links for a specific user """

        user_id = request.GET['user_id']
        records = ReportEntry.objects.filter(report__user_id=user_id)
        return JsonResponse({'links': [record.img.url for record in records]})
