# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser



class Customer(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class User(AbstractUser):
    """ Extends django built-in user model """
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)


class Machine(models.Model):
    """ Every machine is own by a customer """
    name = models.CharField(max_length=50)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class CropType(models.Model):
    name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name


class CropTrait(models.Model):
    crop = models.ForeignKey(CropType, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.crop) + ' '+ self.name


class Report(models.Model):
    crop_trait = models.ForeignKey(CropTrait, on_delete=models.CASCADE)
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)


def get_upload_path(instance, filename):
    # Save as machine_id/user_id/report_id/crop_trait_id
    report = instance.report
    return "machine_%s/user_%s/report_%s/trait_%s/%s" % (report.machine_id, report.user_id, report.id, report.crop_trait_id, filename)


class ReportEntry(models.Model):
    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    img = models.FileField(upload_to=get_upload_path)
    answer = models.CharField(max_length=100)  # answer of the ML model for this img