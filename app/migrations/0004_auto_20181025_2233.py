# Generated by Django 2.1.1 on 2018-10-25 19:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20181025_2230'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='customer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app.Customer'),
            preserve_default=False,
        ),
    ]
