from django.contrib import admin
from .models import Report, ReportEntry, Machine, User, Customer, CropType, CropTrait


models = [Report, ReportEntry, Machine, User, Customer, CropType, CropTrait]

for model in models:
    admin.site.register(model)
